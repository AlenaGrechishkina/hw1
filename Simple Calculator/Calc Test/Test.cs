﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simple_Calculator;

namespace Calc_Test
{    
    
     [TestFixture]
    public class Test
    {
        Calc calc = new Calc();
        [Test]
        public void Add()
        {
            int num1 = 1, num2 = 2;
            Assert.AreEqual(calc.Add(num1,num2), 3);
          }
         [Test]
        public void Subtract()
        {
            int num1 = 3, num2 = 2;
            Assert.AreNotEqual(calc.Subtract(num1,num2), 2);
        }
        [Test]
        public void Divide()
        {
            int num1 = 4, num2 = 2;
            Assert.Positive(calc.Divide(num1,num2));
        }
        [Test]
        public void Multiply()
        {
            int num1 = 4, num2 = 0;
            Assert.Zero(calc.Multiply(num1,num2));
        }

        [Test]
        public void NegAdd()
        {
            int num1 = 0, num2 = 0;
            Assert.NotZero(calc.Add(num1,num2));
        }
        [Test]
        public void NegSubstract()
        {
            int num1 = 3, num2 = 2;
            Assert.Negative(calc.Subtract(num1,num2));
        }
        [Test]
        public void NegDivide()
        {
            int num1 = 4, num2 = 2;
            Assert.AreSame(calc.Divide(num1,num2), 5);
        }
        [Test]
        public void NegMultiply()
        {
            int num1 = 4, num2 = 2;
            Assert.Greater(calc.Multiply(num1,num2), 9);
        }

    }
}
